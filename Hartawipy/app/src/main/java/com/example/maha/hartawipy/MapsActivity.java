package com.example.maha.hartawipy;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            MapsActivity.lastLocation = location;
            MapsActivity.lastLocation=location;
           // Log.v("TAG", "IN ON LOCATION CHANGE, lat=" + latitude + ", lon=" + longitude);

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @SuppressLint("MissingPermission")
        @Override
        public void onProviderEnabled(String s) {
            Criteria criteria = new Criteria();
            String bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();

            locationManager.requestLocationUpdates(bestProvider, 1000, 0, this);
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };
    public static Context currentAct;
    public static Location lastLocation;
    public  LocationManager locationManager;
    public GoogleMap mMap;
    private ArrayList<Point> points = new ArrayList<Point>();
    private SQLiteDatabase mydatabase;
    public void setupGPS()
    {
        ActivityCompat.requestPermissions(MapsActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            System.out.println("No permission");
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListenerGPS);

    }

    public void insertInDb(Point p)
    {
        String query="Insert into Readings Values (";
        Cursor c = mydatabase.rawQuery("SELECT * FROM Readings ", null);
        String[] columnNames;
        try {
           columnNames = c.getColumnNames();
        } finally {
            c.close();
        }
        for (String field:columnNames
             ) {
            query+="\'"+p.readings.get(field)+"\',";

        }
        query=query.substring(0,query.length()-1);
        query+=")";
    Log.e("Query is",query);


    mydatabase.execSQL(query);

    }
    public List<Point>  getAllPoints()
    {
        Cursor resultSet = mydatabase.rawQuery("Select * from Readings",null);
       resultSet.moveToFirst();
       List<Point> points=new ArrayList<Point>();


      while(resultSet.moveToNext())
        {
            Point p = new Point();

            for (String collumn : resultSet.getColumnNames()) {
                p.add2Readings(collumn,resultSet.getString(resultSet.getColumnIndex(collumn)));
            }
            points.add(p);
        }

        return points;
    }
    public void add_point(Point point) {
        Random rand=new Random();
        Criteria criteria = new Criteria();
        String bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();

        @SuppressLint("MissingPermission") Location myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if(myLocation!=null) {
            //sterge randurile alea cand esti gata
                            // + rand.nextDouble()
            LatLng locationLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude() );
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
            point.add2Readings("Data", currentDateandTime);
            point.add2Readings("Lat",String.valueOf(locationLatLng.latitude));
            point.add2Readings("Long",String.valueOf(locationLatLng.longitude));
            insertInDb(point);


        }
        else
        {

        }
    }

    public Point stringSplitter(String readgins) {

        String[] subStrings = readgins.split(" ");
        Point nouveau = new Point();
        for (int i = 0; i < subStrings.length / 2; i++) {
            nouveau.add2Readings(subStrings[2 * i], subStrings[2 * i + 1]);
        }
        this.points.add(nouveau);
        return nouveau;

    }

    public void start_server(Handler mainHandler) throws IOException {


        String fromClient;

        ServerSocket server = new ServerSocket(8081);
        System.out.println("wait for connection on port 8081");
        Log.e("MyApp", "Started server");
        boolean run = true;
        @SuppressLint("MissingPermission") Location myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        boolean b=false;//set true to print location
        while(b)
        {

            LatLng locationLatLng = new LatLng(myLocation.getLatitude() , myLocation.getLongitude() );

            System.out.println("New pos is" + locationLatLng.latitude + locationLatLng.longitude);

        }

        while (run) {
            Socket client = server.accept();

            Log.e("New connection","got connection on port 8080");
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintWriter out = new PrintWriter(client.getOutputStream(), true);

            while (client.isConnected()) {
                if (in.ready()) {
                    fromClient = in.readLine();

                    if (fromClient != null) {
                        if (fromClient.equals("Ready")) {

                            out.println("Fetch");
                            out.flush();
                            System.out.println("Sending fetch command");

                            String readings = in.readLine();
                            System.out.println(readings);
                            Toast.makeText(MapsActivity.currentAct, "Received new points", Toast.LENGTH_LONG).show();
                            Point aux = stringSplitter(readings);

                            Runnable myRunnable = new Runnable() {
                                Point point;

                                @Override
                                public void run() {
                                    add_point(point);
                                }

                                public Runnable init(Point aux) {
                                    this.point = aux;
                                    return this;
                                }
                            }.init(aux);
                            mainHandler.post(myRunnable);
                            client.close();
                            break;
                        }
                        if (fromClient.equals("End")) {
                            System.out.println("FInished with this client");
                            client.close();
                            break;
                        }

                    }

                }
            }

        }
        System.out.println("Finished listening for clients");
        Log.d("MyApp", "Stopped");

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_area);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setupGPS();
        mydatabase = openOrCreateDatabase("MyR1",MODE_PRIVATE,null);

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Readings(Data DATE,Lat FLOAT,Long FLOAT,Temperature FLOAT,Humidity DOUBLE,Light_blue FLOAT,Light_red FLOAT,Pressure Float,Altitude FLOAT,Dew_point FLOAT);");



    }
    public  void  interpolate(LatLng clicked)
    {
        List<Point> points=this.getAllPoints();
        List<Point> closePoints=new ArrayList<Point>();
        for(Point aux:points)
        {
           Location start=new Location("Start");
           start.setLatitude(clicked.latitude);
           start.setLongitude(clicked.longitude);
           Location stop=new Location("Stop");
           stop.setLatitude(Double.parseDouble(aux.readings.get("Lat")));
            stop.setLongitude(Double.parseDouble(aux.readings.get("Long")));
            double distance=start.distanceTo(stop);
            Log.e("Distance","Distanta e"+String.valueOf(distance));


            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");




            String dataString=aux.readings.get("Data");

            Date date=null;
            try {
                date = sdf.parse(dataString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
           long currentDate=Calendar.getInstance().getTimeInMillis();
            long recordDate=date.getTime();
            if(distance <200 && (currentDate-recordDate<604800000))//mai recent de 7 zile
            {

                aux.readings.remove("Data");
                closePoints.add(aux);
            }

        }
        Point meanValues=computeMean(closePoints);//compute mean and display it
        meanValues.readings.put("Lat",String.valueOf(clicked.latitude));
        meanValues.readings.put("Long",String.valueOf(clicked.longitude));
//display the points
        displayOnMap(closePoints);
        TextView tv1 = (TextView)findViewById(R.id.textBox);
        tv1.setText(meanValues.toString());
    }
    public void displayOnMap(List<Point> points)
    {
        mMap.clear();
        for(Point aux:points)
        {
            MarkerOptions markerOptions = new MarkerOptions();
            LatLng pointPosition=new LatLng(Double.parseDouble(aux.readings.get("Lat")),Double.parseDouble(aux.readings.get("Long")));
            markerOptions.position(pointPosition);



            mMap.addMarker(markerOptions);
        }

    }
    public Point computeMean(List<Point> points)
    {Point averagePoint=new Point();
        if(points.size()>0)
        {

            for(String field:points.get(0).readings.keySet())
            {
                double averaged=0;
                for(Point p:points)
                {
                    double value=Double.parseDouble(p.readings.get(field));
                    int size=points.size();
                    averaged+=value/size;
                }
                averagePoint.add2Readings(field,String.valueOf(averaged));

            }
    }
    return averagePoint;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    public void startServerThread()
    {
        Runnable serverRunnable=new Runnable() {
            @Override
            public void run() {
                try {
                    start_server(new Handler(getApplicationContext().getMainLooper()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread serverThread=new Thread(serverRunnable);
        serverThread.start();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Cluj and move the camera

        mMap.setInfoWindowAdapter(new MyInfoWIndosAdapter());
        // mMap.setMinZoomPreference(13);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46.7793,23.5956),12));
        startServerThread();
        GoogleMap.OnMapClickListener listener=new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                this.clicked=latLng;

                MapsActivity.this.interpolate(clicked);

            }
            private LatLng clicked;
            public LatLng getLocation()
            {

                return clicked;
            }
        };
        mMap.setOnMapClickListener(listener);
        MapsActivity.currentAct=getApplicationContext();
        // mMap.setMinZoomPreference(10.0f);
        //mMap.setMaxZoomPreference(25.0f);



    }

    class MyInfoWIndosAdapter implements GoogleMap.InfoWindowAdapter{
        private final View myContentsView;
        MyInfoWIndosAdapter(){
            myContentsView = getLayoutInflater().inflate(R.layout.map_info, null);
        }
        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView)myContentsView.findViewById(R.id.title));
            tvTitle.setText(marker.getPosition().toString());
            TextView tvSnippet = ((TextView)myContentsView.findViewById(R.id.snippet));
            tvSnippet.setText(marker.getSnippet());

            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }
}
