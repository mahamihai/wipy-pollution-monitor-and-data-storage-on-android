package com.example.maha.hartawipy;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Maha on 3/11/2018.
 */

public class Point implements Serializable {
    public HashMap<String, String> getReadings() {
        return readings;
    }

    public void add2Readings(String key,String value) {
        this.readings.put(key,value);
    }
    @Override
    public String toString()
    {
        StringBuilder formatedString=new StringBuilder("");
        for (String aux :this.readings.keySet())
        {
            formatedString.append(aux+":"+this.readings.get(aux)+"\n");


        }
        return formatedString.toString();
    }


    public HashMap<String,String> readings=new HashMap<String,String>();


}
