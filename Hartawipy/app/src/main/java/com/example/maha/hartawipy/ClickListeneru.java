package com.example.maha.hartawipy;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Maha on 4/15/2018.
 */

public class ClickListeneru implements GoogleMap.OnMapClickListener {
    @Override
    public void onMapClick(LatLng latLng) {
        this.clicked=latLng;

    }
    public LatLng clicked;
    public ClickListeneru(Runnable clickHandler)
    {
        clickHandler.run();
    }
}
