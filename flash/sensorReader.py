from pysense import Pysense
from LIS2HH12 import LIS2HH12
from SI7006A20 import SI7006A20
from LTR329ALS01 import LTR329ALS01
from MPL3115A2 import MPL3115A2,ALTITUDE,PRESSURE
def init_sensors():

    global py
    global mp
    global si
    global lt
    global li
    global mpp
    py = Pysense()
    mpp = MPL3115A2(py,mode=PRESSURE) # Returns pressure in Pa. Mode may also be set to ALTITUDE, returning a value in meters


    mp = MPL3115A2(py,mode=ALTITUDE) # Returns height in meters. Mode may also be set to PRESSURE, returning a value in Pascals
    si = SI7006A20(py)
    lt = LTR329ALS01(py)
    li = LIS2HH12(py)
def form_string(measurements):
    data=""

    for i in range(0,len(measurements)):
        #print("asta e" +measurements[i][0])
        data=data+ ('%s %s' %(measurements[i][0],measurements[i][1])) + " "

    return data
def write_sensor_values():
    global py
    global mp
    global si
    global lt
    global li
    global mpp
    print("MPL3115A2 temperature: " + str(mp.temperature()))
    print("Altitude: " + str(mp.altitude()))
    print("Pressure: " + str(mpp.pressure()))

    print("Temperature: " + str(si.temperature())+ " deg C and Relative Humidity: " + str(si.humidity()) + " %RH")
    print("Dew point: "+ str(si.dew_point()) + " deg C")
    t_ambient = 24.4
    print("Humidity Ambient for " + str(t_ambient) + " deg C is " + str(si.humid_ambient(t_ambient)) + "%RH")

    print("Light (channel Blue lux, channel Red lux): " + str(lt.light()))

    print("Acceleration: " + str(li.acceleration()))
    print("Roll: " + str(li.roll()))
    print("Pitch: " + str(li.pitch()))
    print("Battery voltage: " + str(py.read_battery_voltage()))

def get_values_string():
    global py
    global mp
    global si
    global lt
    global li
    global mpp
    t_ambient=24.4
    measurements=[
    ["Temperature",str(si.temperature())],
    ["Humidity", str(si.humid_ambient(t_ambient))],
    ["Light_blue", str(lt.light()[0])],
    ["Light_red", str(lt.light()[1])],

    ["Pressure",str(mpp.pressure())],
    ["Altitude",str(mp.altitude())],
    ["Dew_point",str(si.dew_point())]
    ]



    data=form_string(measurements=measurements)
    return data
